import os
from dotenv import load_dotenv
load_dotenv("environment_variables/creds.env")
basedir = os.path.abspath(os.path.dirname(__file__))


class config(object):
    run_mode = "term"

    class SQLConfig(object):
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
        SQLALCHEMY_TRACK_MODIFICATIONS = False

    class SiteSQLConfig(object):
        hostname = "database.dottodotstudio.co.uk"
        username = "stockmanager"
        password = "Sookie500kie!"
        databaseName = "dbs201416"

    class wooCommerceConfig(object):
        hostname = "https://www.dottodotstudio.co.uk/"
        consumerKey = "ck_95c503043192e499efb707b0f9f5ea1b940ebab0"
        consumerSecret = "cs_471050e3e744cb8b261a41e3969ce8a388c258ca"
        timeout = 30  # In seconds.

    class squareConfig(object):
        token = "EAAAEKpdqJZFvHON5BeCPRJhEiYOLRy1739IYoo5rLVn91jaZpebctFycE1MaIIQ"
        locationID = "A3CCWAVC79BX2"
        environment = "production"

    class etsyClientConfig(object):
        consumerKey = "g8gseeoqk0kdx1328tl63ydn"
        consumerSecret = "rt4leooi1g"
        oAuthToken = "90540c3d643e27b111f1f18278a1c3"
        oAuthSecret = "ae5c978b56"
        oAuthVersion = "1.0a"

    class etsyWrapperConfig(object):
        shopID = "12703209"
