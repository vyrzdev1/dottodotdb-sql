from . import forms
import npyscreen


class TUIApp(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm("MAIN", forms.topLevelMenu, name="Top Level")
