def userInput(promptStr):
    # Basic userinput, replaces python's own input() function, adds handling for exit and
    # back to menu commands at any point in the program.
    userInputStr = str(input(promptStr))
    if userInputStr.lower() in ["quit", "exit"]:
        pass
    elif userInputStr.lower() in ["cancel"]:
        pass
    else:
        return userInputStr


class menu:
    def __init__(self, app, items):
        self.app = app
        self.items = items
        self.itemsDict = {x.name: x for x in self.items}

    def show(self):
        choice = ""
        choices = dict()
        count = 1
        for x in self.items:
            print(str(count) + ": " + x.name)
            choices[str(count)] = x
            count = count + 1
        while choice not in choices.keys():
            choice = userInput(">> ")
        choice = choices[choice]
        choice.show()


class sequentialMenu:
    def __init__(self, app, items):
        self.items = items

    def show(self):
        collectedData = dict()
        for x in self.items:
            collectedData[x.name] = x.show()
        return collectedData

    def editData(self, collectedData):
        for x in collectedData:
            if x not in self.itemsDict.keys():
                print("ERROR! Extra values which are not known to this menu!!!")
        choice = ""
        choices = dict()
        count = 1
        print("Edit Which Value?")
        for x in collectedData.keys():
            print(str(count) + ": " + x)
            choices[str(count)] = self.itemsDict[x]
        while choice not in choices:
            choice = userInput(">> ")
        choice = choices[choice]
        collectedData[choice.name] = choice.show()
        return collectedData


class inputItem:
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def show(self):
        print("Epic")
