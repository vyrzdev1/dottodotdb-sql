## Models

Each thing that needs to be stored has a model assigned to it, this allows it to be constructed quickly by SQLAlchemy, a library that converts python models to database tables automatically. This library also allows us to model relations between models.

The Database Structure this DBMS uses is EAV. (Entity-Attribute-Value) This means that each entity has a relationship of values, which each have a many-many relationship to their property type. This enables the End-User to add custom attributes, and custom categories.

### Libraries Used

- ActiveAlchemy 
  - A drop in replacement for flask_sqlalchemy, but without dependence on Flask context.
  - Has some issues working with flask_migrate, when prepping a migration swap ActiveAlchemy out for SQLAlchemy.
- flask_migrate
  - A library that combs over your models, and builds the necessary tables in SQL.

---------------------------



## Manufacturer

### Keys

- id: Integer
- name: String
- prefix: String (Used as the start of any products SKU)
- description: String

### Relationships

- products: One to many relationship



### JSON Representation

```json
{
	"id": 1,
	"name": "John Lauden",
	"prefix": "001",
	"description": "A Fabric Manufacturer/Supplier"
}
```

### Python Instantiation

```python
from models import manufacturer

newManufacturer = manufacturer(
    name="John Lauden",
    prefix="001",
    description="A Fabric Manufacturer/Supplier"
)

db.session.add(newManufacturer)
```

----------------------------------------

## Category

### Keys

- id: Integer
- name: string
- description: string

### Relationships

- properties: Many to many relationship
  - Uses a EAV model, thus categories store a record of the applicable properties for them.

- products: One to many relationship



### JSON Representation

```json
{
    "id": 1,
    "name": "Fabric",
    "description": "A Fabric Category",
    "properties": []
}
```

### Python Instantiation

```python
from models import category
from . import property1

newCategory = category(
    name="Fabric",
    description="A Fabric Category"
)

# Adding properties can be done via appending a propertyObject to the relationship:
newCategory.properties.append(property1)

db.session.add(newCategory)
```

----------------------------------------

## Product

### Keys

- id: Integer
- name: String
- sku: String
- manufacturer_sku: String

### Foreign Keys

- manufacturer
  - References the manufacturer object that created it.
- category
  - References the category applicable to the product.

### Relationships

- price
  - References a child **price** object.
- properties
  - Misleadingly named, this relationship references **propertyValue** objects.
  - Following the EAV model, thus propertyName not stored here.

### JSON Representation

```json
{
    "id": 1,
    "name": "Cotton Poplin",
    "sku": "00100001",
    "price": {},
    "manufacturer_sku": "ERT678",
    "manufacturer": {},
    "category": {},
    "properties": []
}
```

### Python Instantiation

```python
from models import product
from . import newCategory, newManufacturer, newPrice, newPropertyValue

newProduct = product(
	name="Cotton Poplin",
    sku="00100001",
    price=newPrice,
    manufacturer_sku="ERT678",
    category=newCategory,
    manufacturer=newManufacturer,
)

newProduct.properties.append(newPropertyValue)

db.session.add(newProduct)
```

----------------------

## Property

### Keys

- id: Integer
- name: String
- description: String
- dataType: String (Used by input boxes for validation.)

### Relationships

- propertyValues
  - A one to many relationship.



### JSON Representation

``` json
{
    "id": 1,
    "name": "property1",
    "description": "A custom property.",
    "dataType": "int"
}
```



### Python Instantiation

``` python
from models import property

property1 = property(
	name="property1",
    description="A custom Property",
    dataType: "int"
)

db.session.add(property1)
```



--------------------

## PropertyValue

### Keys

- id: Integer
- value: String

### Foreign Keys

- product
  - The parent product that this value applies to.
- property
  - The property this value applies to.

### JSON Representation

``` json
{
    "id": 1,
    "value": "12",
}
```



### Python Instantiation

``` python
from models import propertyValue
from . import newProduct, property1

newPropertyValue = propertyValue(
	value="12",
    product=newProduct,
    property=property1,
)

db.session.add(newPropertyValue)
```



-------------

## Money

### Keys

### Relationships

### 	JSON Representation

### Python Instantiation
----------------

