### JSON Format.

The DBMS will primarily take JSON input from the frontends and build objects from them...

#### Example of a BLOB to *create* a product.

```json
{
    "name": "Cotton Poplin",
    "sku": "00100001",
    "price": {
        "amount": 1600,
        "currency": "GBP",
    },
    "manufacturer_sku": "ERT678",
    "manufacturer": {
        	"id": 1
    },
    "category": {
    		"id": 1
	},
    "properties": [
        {
            "property": {
                "id": 1
            },
            "value": 12
        },
        {
            "property": {
                "id": 2
            },
            "value": "epic"
        }
    ]
}
```

#### Example Response

```json
{
    "id": 1,
    "name": "Cotton Poplin",
    "sku": "00100001",
    "price": {
        "amount": 1600,
        "currency": "GBP"
    },
    "manufacturer_sku": "ERT678",
    "manufacturer": 
    	{
        	"id": 1,
            "name": "John Lauden",
            "prefix": "001"
    	},
    "category": 
    	{
    		"id": 1,
            "name": "custom_category",
            "properties": [
                {
                    "id": 1,
                    "name": "customPropertyName",
                    "description": "A custom property",
                    "type": "int",
                    
                },
                {
                    "id": 2,
                    "name": "customPropertyName2",
                    "description": "A custom property (2)",
                    "type": "str",
                }
            ]
		},
    "properties": [
        {
            "id": 1,
            "property": {
                "id": 1,
                "name": "customPropertyName",
                "description": "A custom property",
                "type": "int",
            },
            "value": 12,
        },
        {
            "id": 2,
            "property": {
                "id": 2,
                "name": "customPropertyName2",
                "description": "A custom property (2)",
                "type": "str",
            },
            "value": "epic",
        }
    ]
}
```