from . import APISquare, APIEtsy, APIWooCommerce


class APIHandler:
    def __init__(self, config):
        self.APISquare = APISquare(config)
        self.APIEtsy = APIEtsy(config)
        self.APIWooCommerce = APIWooCommerce(config)
        self.APIArray = [self.APISquare, self.APIEtsy, self.APIWooCommerce]

    def pushProduct(self, productObject):
        for api in self.APIArray:
            api.pushProduct(productObject)
        # TODO: Add error catching.

    # Stock Specific Methods.

    def getStockChanges(self, productObject, interval=30):
        stockChanges = list()
        for api in self.APIArray:
            stockChanges.append(api.getStockChanges(interval))
        # TODO: Add error catching.
        return stockChanges

    def pushStock(self, productObject):
        # TODO: Modify models to store stock values, etc.
        pass
