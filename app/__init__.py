from flask import Flask
from config import config
from active_alchemy import ActiveAlchemy
from flask_migrate import Migrate
from . import productHandler

if config.run_mode == "term" and False:
    db = ActiveAlchemy(config.SQLConfig.SQLALCHEMY_DATABASE_URI)

else:
    app = Flask(__name__)
    app.config.from_object(config.SQLConfig)
    db = ActiveAlchemy(app=app)
    migrate = Migrate(app, db)


class Manager:
    def __init__(self, config):
        self.db = ActiveAlchemy(config.SQLConfig.SQLALCHEMY_DATABASE_URI)
        self.productHandler = productHandler(self.db)
        self.APIHandler = None
        self.stockHandler = None
