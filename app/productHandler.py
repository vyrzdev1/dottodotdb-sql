from models import product, manufacturer, category, propertyValue, money


class productHandler:
    def __init__(self, db):
        self.db = db

    def createProduct(self, json):
        newProduct = product(name=json.get("name"),
                             sku=json.get("sku"),
                             manufacturer_sku=json.get("manufacturer_sku"),
                             )

        newProduct.manufacturer = manufacturer.query.get(json.get("manufacturer").get("id"))
        newProduct.category = category.query.get(json.get("category").get("id"))
        for x in newProduct.category.properties.all():
            newProduct.properties.append(propertyValue(property=x, value=json.get(x.name)))
        newProduct.price = money(amount=json.get("price").get("amount"), currency=json.get("price").get("currency"))

        self.db.add(newProduct)
        self.db.commit()
        return newProduct

    def getProduct(self, id=None, **filters):
        if len(filters.keys()) != 0:
            fetchedProduct = product.query.filter_by(**filters)
        elif id is not None:
            fetchedProduct = product.query.get(id)
        else:
            fetchedProduct = None
            print("Not enough args! Pass id, or filters.")
        return fetchedProduct

    def deleteProduct(self, productObject):
        self.db.session.delete(productObject)
        self.db.session.commit()

    def searchProducts(self):
        pass
