from . import db
categories = db.Table("categories",
                      db.Column("category_ID", db.Integer(), db.ForeignKey("category.ID"), primary_key=True),
                      db.Column("property_ID", db.Integer(), db.ForeignKey("property.ID"), primary_key=True),
                      )


class manufacturer(db.Model):
    # Essential Data
    ID = db.Column(db.Integer(), primary_key=True)
    prefix = db.Column(db.String(3))

    # General Data
    name = db.Column(db.String())

    # Relationships
    products = db.relationship("product", backref="manufacturer", lazy="dynamic")

    def __repr__(self):
        return "<manufacturer: name: {} prefix: {}>".format(self.name, self.prefix)

    def serialize(self):
        json = {
            "ID": self.ID,
            "name": self.name,
            "prefix": self.prefix,
        }
        return json


class category(db.Model):
    ID = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())
    description = db.Column(db.String())
    # Relationships
    properties = db.relationship("property", secondary="categories", backref=db.backref("categorys", lazy=True), lazy="subquery")
    products = db.relationship("product", backref="category", lazy="dynamic")

    def __repr__(self):
        return "<category: name: {}>".format(self.name)

    def serialize(self):
        json = {
            "ID": self.ID,
            "name": self.name,
            "description": self.description,
            "properties": [x.serialize() for x in self.properties]
        }
        return json


class product(db.Model):
    # Essential Data
    ID = db.Column(db.Integer(), primary_key=True)
    sku = db.Column(db.String(8), primary_key=True)
    manufacturerID = db.Column(db.Integer(), db.ForeignKey("manufacturer.ID"))
    categoryID = db.Column(db.Integer(), db.ForeignKey("category.ID"))

    # General Data
    manufacturer_sku = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())

    # Relationships
    price = db.relationship("money", backref="product", lazy="dynamic")
    properties = db.relationship("propertyValue", backref="product", lazy="dynamic")
    stockChanges = db.relationship("stockChange", backref="product", lazy="dynamic")

    def __repr__(self):
        return "<product: name: {} sku: {}>".format(self.name, self.sku)

    def serialize(self):
        json = {
            "ID": self.ID,
            "sku": self.sku,
            "manufacturer_sku": self.manufacturer_sku,
            "name": self.name,
            "price": self.price.serialize(),
            "manufacturer": self.manufacturer.serialize(),
            "category": self.category.serialize(),
            "properties": [x.serialize() for x in self.properties],
        }
        return json


class property(db.Model):
    # Essential Data
    ID = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String())

    # General Data
    description = db.Column(db.String())

    # Relationships
    propertyValues = db.relationship("propertyValue", backref="property", lazy="dynamic")

    def __repr__(self):
        return "<property: name:{}>".format(self.name)

    def serialize(self):
        json = {
            "ID": self.ID,
            "name": self.name,
            "description": self.description,
        }
        return json


class propertyValue(db.Model):
    # Essential Data
    ID = db.Column(db.Integer(), primary_key=True)
    productID = db.Column(db.Integer(), db.ForeignKey("product.ID"))
    propertyID = db.Column(db.Integer(), db.ForeignKey("property.ID"))
    value = db.Column(db.String())

    def __repr__(self):
        return "<propertyValue: property:{} Value:{}>".format(self.property, self.value)

    def serialize(self):
        json = {
            "ID": self.ID,
            "property": self.property.serialize(),
            "value": self.value
        }
        return json


class money(db.Model):
    ID = db.Column(db.Integer(), primary_key=True)
    amount = db.Column(db.Integer())
    currency = db.Column(db.String(3))

    def __repr__(self):
        return "<money: amount:{} currency:{}>".format(self.amount, self.currency)

    def serialize(self):
        json = {
            "amount": self.amount,
            "currency": self.currency
        }
        return json


class stockChange(db.Model):
    ID = db.Column(db.Integer(), primary_key=True)
    productID = db.Column(db.Integer(), db.ForeignKey("product.ID"))
    service = db.Column(db.String())
    changeType = db.Column(db.String())
    quantity = db.Column(db.Numeric())
    time = db.Column(db.String())

    def __repr__(self):
        return "<change: type:{} quantity:{}>".format(self.changeType, self.quantity)

    def serialize(self):
        json = {
            "type": self.type,
            "quantity": self.quantity,
            "time": self.time,
            "service": self.service
        }
        return json
